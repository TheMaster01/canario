import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/notas')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    // {
    //   path: '/notas',
    //   name: 'notas',
    //   component: () => import('./views/notas')
    // },
    {
      path: '/estudiantes',
      name: 'estudiantes',
      component: () => import('./views/estudiantes')
    },
    {
      path: '/materias',
      name: 'materias',
      component: () => import('./views/materias')
    },
    {
      path: '/carreras',
      name: 'carreras',
      component: () => import('./views/carreras')
    },
    {
      path: '/profesores',
      name: 'profesores',
      component: () => import('./views/profesores')
    }
  ]
})
